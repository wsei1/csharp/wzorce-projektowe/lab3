﻿namespace Lab3
{
	public class Part
	{
		public int Id { get; set; }
		public string NameOfPart { get; set; }

		public Part(int id, string nameOfPart)
		{
			Id = id;
			NameOfPart = nameOfPart;
		}
	}
}

﻿namespace Lab3
{
	public interface IProductBuilder
    {
        void BuildName();
        void BuildHead();
        void BuildBody();
        void BuildRightArm();
        void BuildLeftArm();
        void BuildRightLeg();
        void BuildLeftLeg();
        void BuildTail();
        void BuildAllLegs();
    }
}

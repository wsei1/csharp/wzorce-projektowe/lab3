﻿using System;

namespace Lab3
{
	public class Product
    {
        private string Name;
        private string HeadUsage;
        private string BodyUsage;
        private string RightArmUsage;
        private string LeftArmUsage;
        private string RightLegUsage;
        private string LeftLegUsage;
        private string TailUsage;
        private string LegsUsage;

        public void NameOfProduct(string name)
		{
            this.Name = name;
		}

        public void BuildHead(string usage)
		{
            HeadUsage = usage;
		}

        public void BuildBody(string usage)
		{
            BodyUsage = usage;
		}

        public void BuildRightArm(string usage)
		{
            RightArmUsage = usage;
		}

        public void BuildLeftArm(string usage)
		{
            LeftArmUsage = usage;
		}

        public void BuildRightLeg(string usage)
		{
            RightLegUsage = usage;
		}

        public void BuildLeftLeg(string usage)
		{
            LeftLegUsage = usage;
		}

        public void BuildTail(string usage)
		{
            TailUsage = usage;
		}

        public void BuildLegs(string usage)
		{
            LegsUsage = usage;
		} 

        // Actions
        public void NameButton()
        {
            Console.WriteLine("Jestem " + this.Name);
        }

        public void HeadAction()
		{
            Console.WriteLine(HeadUsage);
		}

        public void BodyAction()
		{
            Console.WriteLine(BodyUsage);
		}

        public void RightArmAction()
		{
            Console.WriteLine(RightArmUsage);
		}

        public void LeftArmAction()
        {
            Console.WriteLine(LeftArmUsage);
        }

        public void RightLegAction()
		{
            Console.WriteLine(RightLegUsage);
		}

        public void LeftLegAction()
        {
            Console.WriteLine(LeftLegUsage);
        }

        public void LegsAction()
		{
            Console.WriteLine(LegsUsage);
		}

        public void TailAction()
        {
            Console.WriteLine(TailUsage);
        }
    }
}

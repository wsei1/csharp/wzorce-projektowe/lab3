﻿using System;
using System.Collections.Generic;

namespace Lab3
{
	public class Bear : IProductBuilder
	{
		private Product product = new Product();
		private List<Part> listOfParts = new List<Part>();


		public bool MethodPicker(int choice)
		{
			switch (choice)
			{
				case 1:
					product.NameButton();
					return true;
				case 2:
					product.RightArmAction();
					return true;
				case 3:
					product.LeftArmAction();
					return true;
				case 4:
					product.LegsAction();
					return true;
				case 5:
				default:
					Console.WriteLine("Zakończono program.");
					return false;
			}
		}

		public void BuildName()
		{
			product.NameOfProduct("Niedźwiadek");
			listOfParts.Add(new Part(1, "Głowa - nazwa zabawki"));
		}

		public void BuildHead()
		{
			product.BuildHead("Patrzy się.");
		}

		public void BuildBody()
		{
			product.BuildBody("Warczenie");
		}

		public void BuildRightArm()
		{
			product.BuildRightArm("Prawa łapka rusza się.");
			listOfParts.Add(new Part(2, "Prawa łapka"));
		}

		public void BuildLeftArm()
		{
			product.BuildLeftArm("Lewa łapka rusza się.");
			listOfParts.Add(new Part(3, "Lewa łapka"));
		}

		public void BuildRightLeg()
		{
			product.BuildRightLeg("Prawa nóżka rusza się.");
		}

		public void BuildLeftLeg()
		{
			product.BuildLeftLeg("Lewa nóżka rusza się.");
		}

		public void BuildTail()
		{
			product.BuildTail("Ogon rusza się.");
		}

		public void BuildAllLegs()
		{
			product.BuildLegs("Bieganie.");
			listOfParts.Add(new Part(4, "Wszystkie łapy"));
		}

		public Product GetProduct()
		{
			return product;
		}

		public List<Part> GetParts()
		{
			return listOfParts;
		}
	}
}

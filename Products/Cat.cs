﻿using System;
using System.Collections.Generic;

namespace Lab3
{
	public class Cat : IProductBuilder
	{
		private Product product = new Product();
		private List<Part> listOfParts = new List<Part>();

		public bool MethodPicker(int choice)
		{
			switch (choice)
			{
				case 1:
					product.NameButton();
					return true;
				case 2:
					product.LegsAction();
					return true;
				case 3:
					product.HeadAction();
					return true;
				case 4:
					product.TailAction();
					return true;
				case 5:
				default:
					Console.WriteLine("Zakończono program.");
					return false;
			}
		}

		public void BuildName()
		{
			product.NameOfProduct("Kotek");
			listOfParts.Add(new Part(1, "Głowa - nazwa zabawki"));
		}
		
		public void BuildHead()
		{
			product.BuildHead("Patrzy się");
			listOfParts.Add(new Part(3, "Głowa"));
		}

		public void BuildBody()
		{
			product.BuildBody("Miau miau miau");
		}

		public void BuildRightArm()
		{
			product.BuildRightArm("Prawa łapka rusza się.");
		}

		public void BuildLeftArm()
		{
			product.BuildLeftArm("Lewa łapka rusza się.");
		}

		public void BuildRightLeg()
		{
			product.BuildRightLeg("Prawa nóżka rusza się.");
		}

		public void BuildLeftLeg()
		{
			product.BuildLeftLeg("Lewa nóżka rusza się.");
		}

		public void BuildTail()
		{
			product.BuildTail("Ogon rusza się.");
			listOfParts.Add(new Part(4, "Ogon"));
		}

		public void BuildAllLegs()
		{
			product.BuildLegs("Idzie.");
			listOfParts.Add(new Part(2, "Łapki"));
		}

		public Product GetProduct()
		{
			return product;
		}

		public List<Part> GetParts()
		{
			return listOfParts;
		}
	}
}

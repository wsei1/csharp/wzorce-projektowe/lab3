﻿using System;
using System.Collections.Generic;

namespace Lab3
{
	public class Kangaroo : IProductBuilder
	{
		private Product product = new Product();
		private List<Part> listOfParts = new List<Part>();

		public bool MethodPicker(int choice)
		{
			switch (choice)
			{
				case 1:
					product.NameButton();
					return true;
				case 2:
					product.BodyAction();
					return true;
				case 3:
					product.LegsAction();
					return true;
				case 4:
					product.HeadAction();
					return true;
				case 5:
				default:
					Console.WriteLine("Zakończono program.");
					return false;
			}
		}

		public void BuildName()
		{
			product.NameOfProduct("Kangurek");
			listOfParts.Add(new Part(1, "Głowa - nazwa zabawki"));
		}

		public void BuildHead()
		{
			product.BuildHead("Patrzy się.");
			listOfParts.Add(new Part(4, "Głowa - patrzenie"));
		}

		public void BuildBody()
		{
			product.BuildBody("Spanie.");
			listOfParts.Add(new Part(2, "Tułów"));
		}

		public void BuildRightArm()
		{
			product.BuildRightArm("Rusza łapką.");
		}

		public void BuildLeftArm()
		{
			product.BuildLeftArm("Rusza łapką.");
		}

		public void BuildRightLeg()
		{
			product.BuildRightLeg("Prwa łapa rusza się.");
		}

		public void BuildLeftLeg()
		{
			product.BuildLeftLeg("Lewa łapa rusza się.");
		}

		public void BuildTail()
		{
			product.BuildTail("Ogon rusza się.");

		}

		public void BuildAllLegs()
		{
			product.BuildLegs("Skakanie.");
			listOfParts.Add(new Part(3, "Wszystkie łapy"));
		}

		public Product GetProduct()
		{
			return product;
		}

		public List<Part> GetParts()
		{
			return listOfParts;
		}
	}
}

﻿using System;
using System.Collections.Generic;

namespace Lab3
{
	public class Dog : IProductBuilder
	{
		private Product product = new Product();
		private List<Part> listOfParts = new List<Part>();

		public bool MethodPicker(int choice)
		{
			switch (choice)
			{
				case 1:
					product.NameButton();
					return true;
				case 2:
					product.HeadAction();
					return true;
				case 3:
					product.TailAction();
					return true;
				case 4:
					product.BodyAction();
					return true;
				case 5:
				default:
					Console.WriteLine("Zakończono program.");
					return false;
			}
		}

		public void BuildName()
		{
			product.NameOfProduct("Piesek");
			listOfParts.Add(new Part(1, "Głowa - nazwa zabawki"));
		}

		public void BuildHead()
		{
			product.BuildHead("Patrzy się.");
			listOfParts.Add(new Part(4, "Głowa - patrzenie"));
		}
		public void BuildBody()
		{
			product.BuildBody("Hau hau hau");
			listOfParts.Add(new Part(2, "Tułów"));
		}

		public void BuildRightArm()
		{
			product.BuildRightArm("Odgania łapą muchy.");
		}

		public void BuildLeftArm()
		{
			product.BuildLeftArm("Odgania łapą muchy.");
		}

		public void BuildRightLeg()
		{
			product.BuildRightLeg("Prwa łapa rusza się.");
		}

		public void BuildLeftLeg()
		{
			product.BuildLeftLeg("Lewa łapa rusza się.");
		}

		public void BuildTail()
		{
			product.BuildTail("Merda ogonem. Ogon rusza się.");
			listOfParts.Add(new Part(3, "Ogon"));
		}

		public void BuildAllLegs()
		{
			product.BuildLegs("Bieganie.");
		}

		public Product GetProduct()
		{
			return product;
		}

		public List<Part> GetParts()
		{
			return listOfParts;
		}
	}
}

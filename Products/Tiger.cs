﻿using System;
using System.Collections.Generic;

namespace Lab3
{
	public class Tiger : IProductBuilder
	{
		private Product product = new Product();
		private List<Part> listOfParts = new List<Part>();

		public bool MethodPicker(int choice)
		{
			switch (choice)
			{
				case 1:
					product.NameButton();
					return true;
				case 2:
					product.RightArmAction();
					return true;
				case 3:
					product.LegsAction();
					return true;
				case 4:
					product.TailAction();
					return true;
				case 5:
				default:
					Console.WriteLine("Zakończono program.");
					return false;
			}
		}

		public void BuildName()
		{
			product.NameOfProduct("Tygrysek");
			listOfParts.Add(new Part(1, "Głowa - nazwa zabawki"));
		}

		public void BuildHead()
		{
			product.BuildHead("Patrzy się.");
		}

		public void BuildBody()
		{
			product.BuildBody("Warczenie");
		}

		public void BuildRightArm()
		{
			product.BuildRightArm("Odgania łapą muchy.");
			listOfParts.Add(new Part(2, "Łapka"));
		}

		public void BuildLeftArm()
		{
			product.BuildLeftArm("Odgania łapą muchy.");
		}

		public void BuildRightLeg()
		{
			product.BuildRightLeg("Prwa łapa rusza się.");
		}

		public void BuildLeftLeg()
		{
			product.BuildLeftLeg("Lewa łapa rusza się.");
		}

		public void BuildTail()
		{
			product.BuildTail("Ogon rusza się.");
			listOfParts.Add(new Part(4, "Ogon"));
		}

		public void BuildAllLegs()
		{
			product.BuildLegs("Bieganie.");
			listOfParts.Add(new Part(3, "Wszystkie łapy"));
		}

		public Product GetProduct()
		{
			return product;
		}

		public List<Part> GetParts()
		{
			return listOfParts;
		}
	}
}

﻿namespace Lab3
{
	class FactoryDirector
	{
		public void BuildProduct(IProductBuilder product)
		{
			product.BuildName();

			product.BuildHead();
			product.BuildBody();

			product.BuildLeftArm();
			product.BuildRightArm();

			product.BuildLeftLeg();
			product.BuildRightLeg();

			product.BuildTail();

			product.BuildAllLegs();
		}
	}
}

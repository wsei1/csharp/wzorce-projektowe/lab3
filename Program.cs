﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab3
{
	class Program
	{
		static void Main(string[] args)
		{
			FactoryDirector director = new FactoryDirector();
			var bear = new Bear();
			var tiger = new Tiger();
			var dog = new Dog();
			var cat = new Cat();
			var kangaroo = new Kangaroo();

			List<(int, string)> toys = new List<(int, string)>
			{
				(1, "Niedźwiadek"),
				(2, "Tygrysek"),
				(3, "Piesek"),
				(4, "Kotek"),
				(5, "Kangurek")
			};
			Console.WriteLine("Zabawa w ZOO!");
			Console.WriteLine("Jaką zabawkę chcesz stworzyć?");
			for (int i = 0; i < toys.Count; i++)
			{
				Console.WriteLine(toys[i].Item1 + ". " + toys[i].Item2);
			}

			var toyChoice = int.Parse(Console.ReadLine());

			if (!toys.Any(x => x.Item1 == toyChoice))
			{
				Console.WriteLine("Wybrałeś złą liczbę.");
				return;
			}
			var choice = 0;
			var isContinue = true;
			switch (toyChoice)
			{
				case 1:
					Product bearProduct = bear.GetProduct();
					director.BuildProduct(bear);
					var bearParts = bear.GetParts().OrderBy(x => x.Id);
					while (isContinue)
					{
						DisplayMenu("niedźwiadka", bearParts);
						choice = int.Parse(Console.ReadLine());
						Console.Clear();
						isContinue = bear.MethodPicker(choice);
					}
					break;
				case 2:
					Product tigerProduct = tiger.GetProduct();
					director.BuildProduct(tiger);
					var tigerParts = tiger.GetParts().OrderBy(x => x.Id);
					while (isContinue)
					{
						DisplayMenu("tygryska", tigerParts);
						choice = int.Parse(Console.ReadLine());
						Console.Clear();
						isContinue = tiger.MethodPicker(choice);
					}
					break;
				case 3:
					Product dogProduct = dog.GetProduct();
					director.BuildProduct(dog);
					var dogParts = dog.GetParts().OrderBy(x => x.Id);
					while (isContinue)
					{
						DisplayMenu("pieska", dogParts);
						choice = int.Parse(Console.ReadLine());
						Console.Clear();
						isContinue = dog.MethodPicker(choice);
					}
					break;
				case 4:
					Product catProduct = cat.GetProduct();
					director.BuildProduct(cat);
					var catParts = cat.GetParts().OrderBy(x => x.Id);
					while (isContinue)
					{
						DisplayMenu("kotka", catParts);
						choice = int.Parse(Console.ReadLine());
						Console.Clear();
						isContinue = cat.MethodPicker(choice);
					}
					break;
				case 5:
					Product kangarooProduct = kangaroo.GetProduct();
					director.BuildProduct(kangaroo);
					var kangarooParts = kangaroo.GetParts().OrderBy(x => x.Id);
					while (isContinue)
					{
						DisplayMenu("kangurka", kangarooParts);
						choice = int.Parse(Console.ReadLine());
						Console.Clear();
						isContinue = kangaroo.MethodPicker(choice);
					}
					break;
				default:
					break;
			}
			Console.ReadKey();
		}

		private static void DisplayMenu(string name, IEnumerable<Part> parts)
		{
			Console.WriteLine($"\nWybrałeś {name}.");
			foreach (var part in parts)
			{
				Console.WriteLine(part.Id + ". " + part.NameOfPart);
			}
			Console.WriteLine("5. Zamknij program");
			Console.WriteLine();
		}
	}
}
